function regChampagne() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale1" style="color: #dbe29c;">C</p><div id="liste"><ul><li>Chardonnay</li><li>Pinot noir</li></ul></div><p id="region" style="color: #dbe29c;">Champagne</p>';
}
function regAlsace() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale1" style="color: #f2ff7c;">A</p><div id="liste"><ul><li>Gewurtztraminer</li><li>Reisling</li><li>Pinot</li></ul></div><p id="region" style="color: #f2ff7c;">Alsace</p>';
}
function regLoire() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale1" style="color: #fab9a5;">L</p><div id="liste"><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul></div><p id="region" style="color: #fab9a5;">Loire</p>';
}
function regBourgogne() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale1" style="color: #d0477a;">B</p><div id="liste"><ul><li>Pinot noir</li><li>Chardonnay</li><li>Gamay</li></ul></div><p id="region" style="color: #d0477a;">Bourgogne</p>';
}
function regJura() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale1" style="color: #ec86b1;">J</p><div id="liste"><ul><li>Pinot noir</li><li>Chardonnay</li><li>Savagnin</li></ul></div><p id="region" style="color: #ec86b1;">Jura</p>';
}
function regAuvergne() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale1" style="color: #e5f07f;">A</p><div id="liste"><ul><li>Pinot noir</li><li>Chardonnay</li><li>Gamay</li></ul><ul><li>Sauvignon</li></ul></div><p id="region" style="color: #e5f07f;">Auvergne</p>';
}
function regSavoie() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale1" style="color: #e1a08c;">S</p><div id="liste"><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul></div><p id="region" style="color: #e1a08c;">Savoie</p>';
}
function regBordeaux() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale1" style="color: #a4335d;">B</p><div id="liste"><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul></div><p id="region" style="color: #a4335d;">Bordeaux</p>';
}
function regLanguedoc() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale2" style="color: #9e073f;">LR</p><div id="liste"><ul><li>Chardonnay</li><li>Cinsaut</li><li>Carignan</li></ul><ul><li>Grenache</li><li>Merlot</li><li>Cabernet-Sauvignon</li></ul></div><p id="region" style="color: #9e073f;">Languedoc - Roussillon</p>';
}
function regProvenceAC() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale1" style="color: #f3ccc0;">P</p><div id="liste"><ul><li>Grenache</li><li>Cinsaut</li><li>Carignan</li></ul><ul><li>Ugni blanc</li></ul></div><p id="region" style="color: #f3ccc0;">Provence - Alpes - Côte d\'Azur</p>';
}
function regCorse() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale1" style="color: #e80c5e;">C</p><div id="liste"><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul></div><p id="region" style="color: #e80c5e;">Corse</p>';
}
function regBeaujolais() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale1" style="color: #fab9a5;">B</p><div id="liste"><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul></div><p id="region" style="color: #fab9a5;">Beaujolais</p>';
}
function regVdr() {
    document.getElementById("bot").innerHTML = '<img id="trait" src="img/trait.png"/><img id="cercle" src="img/cercle.png"/><p id="initiale2" style="color: #fab9a5;">VR</p><div id="liste"><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul><ul><li>Grenache</li><li>Grenache</li><li>Grenache</li></ul></div><p id="region" style="color: #fab9a5;">Vallée du Rhône</p>';
}